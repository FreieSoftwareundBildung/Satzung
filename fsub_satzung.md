![](logo_RGB_21x3cm_300dpi.tif)\ 


__Satzung Freie Software und Bildung e.V. (FSuB e.V.)__

 

In der Fassung vom 05.06.1999 - eingetragen am 16.06.1999  
- geändert am 22.11.2000  
- geändert am 29.11.2008  
- redaktionell geändert 26.9.2018 

__§ 1 Name und Sitz__

Der Verein trägt den Namen "Freie Software und Bildung e.V." (FSuB
e.V.). Sein Sitz ist Berlin. Er ist in das Vereinsregister einzutragen.

__§ 2 Vereinszweck__  

Der Verein fördert - ideell und materiell an Schulen und
Bildungseinrichtungen für Lehrer und Schüler sowie im Internet auch für
den Einzelnen -

-   alles, was dem Verständnis und dem selbstbestimmten Umgang mit den
    neuen Informations- und Kommmunikationstechnologien auf der Basis
    freier Software und offener Standards dient und

-   alles, was geeignet ist, dem Recht auf Selbstbestimmung über die
    eigenen Daten und dem Schutz dieser Daten Geltung zu verschaffen
    sowie die Freiheit der Information und die Freiheit der
    Kommunikation eines jeden Einzelnen zu sichern und zu fördern.

Zu diesem Zweck fördert der Verein insbesondere

-   das Herausstellen von Fähigkeiten, die erforderlich sind, um in
    einer Informations- und Kommunikationsgesellschaft selbstbestimmt
    leben und an dieser teilnehmen zu können,

-   das Bereitstellen von Software und Informationen, die diesen Zielen
    dienen,

-   Projekte in Form von Tagungen, Kursen, Vorträgen, Workshops,
    Arbeitsaufträgen, etc. zur Qualifizierung von Multiplikatoren und
    zur Erstellung von Materialien (Programmen, Anleitungen, ...).
 

__§ 3 Gemeinnützigkeit__
 
Der Verein verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke
im Sinne des Abschnitts "steuerbegünstigte Zwecke" im Sinne der
Abgabenordnung. Der Verein ist selbstlos tätig und verfolgt nicht in
erster Linie eigenwirtschaftliche Zwecke. Mittel dürfen nur für die
satzungsmäßigen Zwecke verwendet werden. Es darf keine Person durch
Ausgaben, die dem Zweck des Vereins fremd sind oder durch
unverhältnismäßig hohe Vergütungen begünstigt werden. Die Mitglieder
erhalten keine Zuwendungen aus Mitteln des Vereins.

__§ 4 Mitgliedschaft__

Mitglied kann jede natürliche und juristische Person werden, die bereit
ist, die Zwecke des Vereins zu unterstützen. Auch nicht rechtsfähige
Vereine und Gesellschaften (Firmen) können die Mitgliedschaft erwerben.
Der Antrag auf Aufnahme muß schriftlich erfolgen. Über die Aufnahme in
den Verein entscheidet der Vorstandsvorsitzende und seine beiden
Stellvertreter. Die Mitgliedschaft endet durch Austritt, Tod oder
förmlicher Ausschließung auf Beschluß des Vorstandes. Der Austritt ist
dem Verein schriftlich mitzuteilen; er kann nur zum Ende des
Kalenderjahres erklärt werden. Diese Erklärung muß spätestens am 30.
November des Austrittsjahres eingegangen sein.

__§ 5 Beiträge und Geschäftsjahr__

Der Jahresbeitrag der ordentlichen Mitglieder wird von der
Mitgliederversammlung festgesetzt; er ist bis zum 31. Januar im Ganzen
zu zahlen. Der Jahresbeitrag der fördernden Mitglieder wird vom Vorstand
festgelegt. Das Geschäftsjahr läuft vom 1.1. bis 31.12. jeden Jahres.

\newpage

__§ 6 Organe und Gliederung__

Organe des Vereins sind:

-   Mitgliederversammlung,
-   der Vorstand.

Die Mitgliederversammlung und der Vorstand geben sich jeweils eine
Geschäftsordnung. Der Vorstand besteht aus dem 1. Vorsitzenden, zwei
Stellvertretern und vier weiteren Vorstandsmitgliedern. Der 1.
Vorsitzende und seine zwei Stellvertreter sind der geschäftsführende
Vorstand im Sinne des 26 des BGB. Der Verein wird gerichtlich und
außergerichtlich durch den ersten Vorsitzenden und einen der beiden
Stellvertreter gemeinsam vertreten. Die Mitglieder des Gesamtvorstandes
werden durch die Hauptversammlung auf drei Jahre gewählt und bleiben bis
zur Neuwahl, jedoch spätestens bis zur nächsten Hauptversammlung, im
Amt. Wiederwahl ist zulässig.

__§ 7 Rechte und Pflichten des Vorstandes__

Dem geschäftsführenden Vorstand obliegt die Geschäftsleitung, die
Ausführung der Vereinsbeschlüsse und die Verwaltung des
Vereinsvermögens. Er kann für die laufenden Geschäfte einen
Geschäftsführer bestellen.

Der geschäftsführende Vorstand kann seine Beschlüsse auch schriftlich,
per Email oder telefonisch fassen. Alle Vorstandsbeschlüsse sind
schriftlich zu dokumentieren und von allen drei geschäftsführenden
Vorstandsmitgliedern eigenhändig zu unterzeichnen.

Der geschäftsführende Vorstand kann zu seiner Beratung für die Dauer
seiner Wahlzeit einen Beirat berufen, dem bis zu sieben Mitglieder
angehören. Hierbei sollen die Vorschläge der Mitgliederversammlung
berücksichtigt werden.

Der Vorsitzende beruft und leitet die Mitgliederversammlung. Hierbei
wird er von einem Tagungsausschuß unterstützt.

Über die Beschlüsse des geschäftsführenden Vorstandes wird ein Protokoll
erstellt, das vom Protokollführer und dem jeweiligen Leiter der Sitzung
zu unterzeichnen ist.

Die Mitglieder des Vorstandes haben keinen Anspruch auf Vergütung ihrer
Tätigkeit.

__§ 8 Mitgliederversammlung__

Alljährlich soll eine Hauptversammlung (ordentliche
Mitgliederversammlung) stattfinden. Zu ihrer Tagesordnung gehören
regelmäßig der Jahresbericht des Vorstandes, der Bericht der beiden
jeweils für das laufende Geschäftsjahr gewählten Kassenprüfer, die
Entlastung des Vorstandes, die Ersatzwahlen der Mitglieder des
Vorstandes und der Kassenprüfer, deren Amtszeit abgelaufen ist, sowie
die Bestimmung des Ortes der nächsten Hauptversammlung. Die
Mitgliederversammlung kann beschließen, die Bestimmung des Ortes der
nächsten Hauptversammlung dem Vorstand zu übertragen.

Die Mitgliederversammlung ist ohne Rücksicht auf die Zahl der
Erschienenen beschlußfähig, wenn die Mitglieder mindestens 30 Tage vor
der Versammlung durch Rundschreiben eingeladen worden sind. Das
Rundschreiben kann in Form einer Email versandt werden.

Die Beschlüsse der Mitgliederversammlung werden durch einfache
Stimmenmehrheit gefaßt, soweit nicht durch Gesetz oder durch diese
Satzung eine andere Mehrheit vorgeschrieben ist. Bei Stimmengleichheit
entscheidet die Stimme des Vorsitzenden, im Falle von Wahlen das Los.

Über die Beschlüsse der Versammlung wird ein Protokoll erstellt, das vom
Protokollführer und dem jeweiligen Leiter der Versammlung zu
unterzeichnen ist.

Satzungsänderungen bedürfen der 2/3-Mehrheit der abgegebenen gültigen
Stimmen.

Über die Auflösung des Vereins kann nur beschlossen werden, wenn sie als
Punkt der Tagesordnung zugleich mit der Einladung zur Hauptversammlung
bekanntgegeben worden ist. Dieser Beschluß bedarf einer Mehrheit von
mindestens 3/4 der anwesenden Mitglieder. Im Falle der Auflösung des
Vereins oder beim Wegfall seines bisherigen Zweckes wird das Vermögen
der gemeinnützigen Umweltstiftung in Osnabrück (Deutsche Bundesstiftung
Umwelt, Finanzamt Osnabrück-Stadt, Steuernummer 66/270/04848) für Zwecke
im Sinne der Vereinsbestrebungen übereignet, die es unmittelbar und
ausschließlich für gemeinnützige, mildtätige oder kirchliche Zwecke zu
verwenden hat.

\newpage

__§ 9 Wirtschaftsführung und Rechnungslegung__

Keine Person, die im Namen des Vereins auftritt, darf Ausgaben tätigen
oder Verpflichtungen eingehen, die nicht durch den aktuellen Stand an
frei verfügbaren und noch nicht zweckgebundenen Geldmitteln gedeckt sind.

Alle Verpflichtungen, die der Verein eingeht, müssen durch eine an
diesen Zweck gebundene Geldanlage in gleicher Höhe gesichert sein. Die
zweckgebundene Geldanlage kann durch eine Bankbürgschaft ersetzt werden,
die folgende Kriterien erfüllt: Die Bürgschaft muß unbefristet,
selbstschuldnerisch, unwiderruflich, unbedingt, einredenfrei und auf
erste Anforderung hin fällig sein. Ein eventueller Hinterlegungsvermerk
ist zu streichen.

Die Rechnungslegung erfolgt in Form der doppelten Buchführung.
Anschaffungen, auch mit längerer Lebensdauer als einer Rechnungsperiode,
sind sofort voll bis auf einen Erinnerungswert von 1.- Euro
abzuschreiben. Kosten und Erträge sind - soweit dies möglich und
wirtschaftlich sinnvoll ist - projektbezogen auf jeweils eigenen sich
entsprechenden Konten zu erfassen. Alle zweckgebundenen Gelder und
Bürgschaften sowie alle eingegangenen Verpflichtungen sind auf jeweils
eigenen Konten zu erfassen.

Aus den Bezeichnungen dieser sich entsprechenden zweckgewidmeten
Vermögens- und Schuldkonten (Bestandskonten) sowie dieser sich
entsprechenden projektbezogenen Kosten- und Ertragskonten (Prozeßkonten)
muß die gegenseitige Zuordnung deutlich erkennbar sein.

Alle buchungsrelevanten Vorgänge sind durch Originalbelege zu
dokumentieren. Aus den Belegen muß hervorgehen, von wem - was und aus
welchem Grund - an wen geflossen oder übergeben worden ist.

Alle Zahlen und Informationen sind mitgliederöffentlich. Nach Abschluß
der Wirtschaftsperiode ist den Mitgliedern eine Zusammenfassung der
Bestands- und der Prozeßkonten zugänglich zu machen.

Die vorstehende Satzung wurde in der Gründungsversammlung vom 05.06.1999
errichtet.
