#!/bin/sh
# Abfrage in scripten
[ -z $1 ] && echo Aufruf $0 Dateiname.md angeben && exit

pandoc --from=markdown "$1" \
	--output="$1".pdf \
	--variable=geometry:"a4paper, \
	top=20mm, \
	left=25mm, \
	right=15mm, \
	bottom=12mm, \
	headsep=10mm, \
	footskip=10mm"  \
	--highlight-style=espresso \
	--pdf-engine=xelatex

exit
